﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace RushHour
{
    /// <summary>
    /// wall object
    /// </summary>
    public class Fal : JatekElem
    {      
        /// <summary>
        /// constructor of a wall
        /// </summary>
        public Fal()
        {           
            Szin = Brushes.LightGray;
        }
    }
}
