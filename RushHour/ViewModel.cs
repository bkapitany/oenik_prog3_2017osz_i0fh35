﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Shapes;

namespace RushHour
{   
    /// <summary>
    /// Viewmodel interactions
    /// </summary>
    public class ViewModel : Bindable
    {
        /// <summary>
        /// size of the basic elements
        /// </summary>
        public const int TILESIZE = 70;

        /// <summary>
        /// viewmodel object
        /// </summary>
        private static ViewModel PELDANY;

        /// <summary>
        /// declares the selected map
        /// </summary>
        private int palyaSzam;

        /// <summary>
        /// current map
        /// </summary>
        private Palya aktualispalya;

        /// <summary>
        /// initialize the viewmodel
        /// </summary>
        public ViewModel()
        {            
            Autok = AutoBeolvas();
            Palyak = PalyaBeolvas();
        }

        /// <summary>
        /// Gets or sets the selected map's number
        /// </summary>
        public int PalyaSzam
        {
            get { return palyaSzam; }
            set { palyaSzam = value; }
        }

        /// <summary>
        /// Gets or sets the maps
        /// </summary>
        public List<Palya> Palyak { get; set; }

        /// <summary>
        /// Gets or sets the cars
        /// </summary>
        public List<Auto> Autok { get; set; }

        /// <summary>
        /// Gets or sets the current map
        /// </summary>
        public Palya AktualisPalya
        {
            get
            {
                return aktualispalya;
            }

            set
            {
                if (value != null)
                {
                    this.PalyaSzam = Palyak.IndexOf(value);
                }

                aktualispalya = value;
            }
        }

        /// <summary>
        /// initialize a new viewmodel or gives it back if it already exist
        /// </summary>
        /// <returns>view model</returns>
        public static ViewModel Get()
        {
            if (PELDANY == null)
            {
                PELDANY = new ViewModel();
            }

            return PELDANY;
        }

        /// <summary>
        /// reads all cars form a txt
        /// </summary>
        /// <returns>all cars</returns>
        private List<Auto> AutoBeolvas()
        {
            List<Auto> autok = new List<Auto>();
            StreamReader sr = new StreamReader("osszes_auto.txt");

            while (!sr.EndOfStream)
            {
                string[] adatok = sr.ReadLine().Split(';');
                Auto uj = new Auto();
                uj.Nev = adatok[0];
                uj.Szin = new SolidColorBrush((Color)ColorConverter.ConvertFromString(adatok[1]));
                uj.Hossz = int.Parse(adatok[2]);
                autok.Add(uj);
            }

            sr.Close();
            return autok;
        }

        /// <summary>
        /// reads all maps from a directory
        /// </summary>
        /// <returns>all maps</returns>
        private List<Palya> PalyaBeolvas()
        {
            List<Palya> palyak = new List<Palya>();

            string[] fajlok = Directory.GetFiles("./palyak", "*.txt");

            foreach (string fajlNev in fajlok)
            {
                Palya palya = new Palya();   
                
                palya.FajlNev = fajlNev;

                StreamReader palyaTxt = new StreamReader(fajlNev);

                palya.Autok = new List<ElhelyezettAuto>();

                while (!palyaTxt.EndOfStream)
                {
                    string[] s = palyaTxt.ReadLine().Split(';');
                    if (s[0] == "HS")
                    {
                        palya.Rekord = int.Parse(s[1]);
                    } 
                    else if (s[0] == "S")
                    {
                        palya.Megoldas = s[1];
                    }
                    else
                    {
                        // masolat az autorol h a tobbi palyan ne zavarjon
                        Auto auto = Autok.Find(x => x.Nev.Contains(s[0]));
                        ElhelyezettAuto elhelyezettAuto;
                        switch (s[3])
                        {
                            case "F":
                                elhelyezettAuto = new ElhelyezettAuto(auto, Irany.Fel, new Point(int.Parse(s[1]), int.Parse(s[2])), palya);
                                break;
                            case "L":
                                elhelyezettAuto = new ElhelyezettAuto(auto, Irany.Le, new Point(int.Parse(s[1]), int.Parse(s[2])), palya);
                                break;
                            case "B":
                                elhelyezettAuto = new ElhelyezettAuto(auto, Irany.Bal, new Point(int.Parse(s[1]), int.Parse(s[2])), palya);
                                break;
                            case "J":
                                elhelyezettAuto = new ElhelyezettAuto(auto, Irany.Jobb, new Point(int.Parse(s[1]), int.Parse(s[2])), palya);
                                break;
                            default:
                                elhelyezettAuto = null;
                                break;
                        }

                        palya.Autok.Add(elhelyezettAuto);
                    }
                }

                palya.Kirajzol();
                palyak.Add(palya);
                palyaTxt.Close();
            }

            return palyak;
        }     
    }
}
