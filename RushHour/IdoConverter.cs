﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace RushHour
{
    /// <summary>
    /// converts milliseconds to readable string
    /// </summary>
    public class IdoConverter : IValueConverter
    {
        /// <summary>
        /// contverting to a readable string
        /// </summary>
        /// <param name="value">object value</param>
        /// <param name="targetType">targe type</param>
        /// <param name="parameter">object parameter</param>
        /// <param name="culture">culture info</param>
        /// <returns>readable string</returns>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            long ms = (long)value;
            string s = (ms / 1000) + ":" + (ms % 1000) + " mp";
            return s;
        }

        /// <summary>
        /// not in use
        /// </summary>
        /// <param name="value">object value</param>
        /// <param name="targetType">targe type</param>
        /// <param name="parameter">object parameter</param>
        /// <param name="culture">culture info</param>
        /// <returns>not implemented exception</returns>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
