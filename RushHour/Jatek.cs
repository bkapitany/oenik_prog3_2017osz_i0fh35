﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace RushHour
{
    /// <summary>
    /// visualization part of the game
    /// </summary>
    public class Jatek : FrameworkElement
    {
        /// <summary>
        /// pen value
        /// </summary>
        private Pen pen = new Pen(Brushes.Black, 1);

        /// <summary>
        /// map value
        /// </summary>
        private Palya palya;

        /// <summary>
        /// size value
        /// </summary>
        private Size meret;

        /// <summary>
        /// starting point
        /// </summary>
        private Point honnanMozog;

        /// <summary>
        /// end point
        /// </summary>
        private Point hovaMozog;

        /// <summary>
        /// ability to move
        /// </summary>
        private bool mozoghat = true;

        /// <summary>
        /// timer value
        /// </summary>
        private DispatcherTimer timer;

        /// <summary>
        /// control value
        /// </summary>
        private ContentControl control;

        /// <summary>
        /// selected item
        /// </summary>
        private ElhelyezettAuto kivalasztott = null;

        /// <summary>
        /// mouse is pressed
        /// </summary>
        private bool lenyomva = false;

        /// <summary>
        /// create a new instance
        /// </summary>
        public Jatek()
        {
            palya = ViewModel.Get().AktualisPalya;
            this.Width = palya.Elemek.GetLength(0) * ViewModel.TILESIZE;
            this.Height = palya.Elemek.GetLength(1) * ViewModel.TILESIZE;
            Loaded += Jatek_Loaded;
            this.InvalidateVisual();
        }

        /// <summary>
        /// rendering the visual elements
        /// </summary>
        /// <param name="drawingContext">drawing context</param>
        protected override void OnRender(DrawingContext drawingContext)
        {
            base.OnRender(drawingContext);

            foreach (JatekElem item in palya.Elemek)
            {
                if (item != default(JatekElem))
                {
                    drawingContext.DrawGeometry(item.Szin, pen, item.Alak);
                }
            }

            var txt = new FormattedText(palya.FajlNev, System.Globalization.CultureInfo.CurrentCulture, FlowDirection.LeftToRight, new Typeface("Courier new"), 21, Brushes.Blue);
            drawingContext.DrawText(txt, new Point(meret.Width / 2, meret.Height / 10));
        }

        /// <summary>
        /// sets up the listeners
        /// </summary>
        /// <param name="sender">sender object</param>
        /// <param name="e">routed event arguments</param>
        private void Jatek_Loaded(object sender, RoutedEventArgs e)
        {
            timer = new DispatcherTimer();
            timer.Interval = TimeSpan.FromMilliseconds(10);
            timer.Tick += Timer_Tick;
            timer.Start();
            control = this.Parent as ContentControl;

            control.MouseMove += Control_MouseMove;
            control.MouseLeftButtonDown += Control_MouseLeftButtonDown;
            control.MouseUp += Control_MouseUp;
            control.MouseRightButtonDown += Control_MouseRightButtonDown;
            control.AllowDrop = true;
        }

        /// <summary>
        /// checking the mouse
        /// </summary>
        /// <param name="sender">object sender</param>
        /// <param name="e">mouse button event arguments</param>
        private void Control_MouseUp(object sender, MouseButtonEventArgs e)
        {
            lenyomva = false;
            mozoghat = true;
            if (kivalasztott != default(ElhelyezettAuto))
            {
                kivalasztott.Kirajzol(kivalasztott.AktualisTombPont);
            }

            kivalasztott = default(ElhelyezettAuto);
        }

        /// <summary>
        /// checking a koordinate
        /// </summary>
        /// <param name="e">mouse button event arguments</param>
        /// <returns>null or an element</returns>
        private JatekElem Keres(MouseButtonEventArgs e)
        {
            JatekElem elem = null;
            Point kurzor = e.GetPosition(this);
            if (kurzor.X >= this.ActualHeight || kurzor.X < 0 ||
                kurzor.Y >= this.ActualWidth || kurzor.Y < 0)
            {
                return elem;
            }

            // visszacserélés mert a tombben keresunk
            int x = (int)kurzor.Y / ViewModel.TILESIZE;
            int y = (int)kurzor.X / ViewModel.TILESIZE;
            elem = palya.Elemek[x, y];
            return elem;
        }

        /// <summary>
        /// checking the mouse
        /// </summary>
        /// <param name="sender">object sender</param>
        /// <param name="e">mouse button event arguments</param>
        private void Control_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            lenyomva = true;
            JatekElem elem = Keres(e);

            if (elem is ElhelyezettAuto)
            {
                kivalasztott = elem as ElhelyezettAuto;
                honnanMozog = e.GetPosition(this);
                hovaMozog = honnanMozog;
            }
        }

        /// <summary>
        /// checking the mouse
        /// </summary>
        /// <param name="sender">object sender</param>
        /// <param name="e">mouse button event arguments</param>
        private void Control_MouseMove(object sender, System.Windows.Input.MouseEventArgs e)
        {
            Point pont = e.GetPosition(this);
            if (pont.X >= 0 || pont.X < this.ActualWidth ||
                pont.Y >= 0 || pont.Y < this.ActualHeight)
            {
                hovaMozog = pont;
            }
        }

        /// <summary>
        /// checking the mouse
        /// </summary>
        /// <param name="sender">object sender</param>
        /// <param name="e">mouse button event arguments</param>
        private void Control_MouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            JatekElem elem = Keres(e);

            if (elem is ElhelyezettAuto)
            {
                MessageBox.Show((elem as ElhelyezettAuto).Nev);
            }
            else if (elem is Fal)
            {
                MessageBox.Show("Fal");
            }
            else if (elem is Kijarat)
            {
                MessageBox.Show("Kijarat");
            }
        }

        /// <summary>
        /// timer method
        /// </summary>
        /// <param name="sender">object sender</param>
        /// <param name="e">event arguments</param>
        private void Timer_Tick(object sender, EventArgs e)
        {            
            if (lenyomva && kivalasztott != null)
            {
                mozoghat = kivalasztott.Mozog(honnanMozog, hovaMozog);
                honnanMozog = hovaMozog;
            }
            
            this.InvalidateVisual();
        }
    }
}