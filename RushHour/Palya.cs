﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace RushHour
{
    /// <summary>
    /// map class
    /// </summary>
    public class Palya
    {
        /// <summary>
        /// file name value
        /// </summary>
        private string fajlNev;

        /// <summary>
        /// solution value
        /// </summary>
        private string megoldas;

        /// <summary>
        /// placed autos
        /// </summary>
        private List<ElhelyezettAuto> autok;

        /// <summary>
        /// intitializes a new map object
        /// </summary>
        public Palya()
        {
            this.Megoldva = false;
            this.Elemek = new JatekElem[8, 8];
        }

        /// <summary>
        /// Gets or sets a value indicating whether the solved property is enabled.
        /// </summary>
        public bool Megoldva { get; set; }

        /// <summary>
        /// Gets or sets record value
        /// </summary>
        public long Rekord { get; set; }

        /// <summary>
        /// Gets or sets 'Elemek' value
        /// </summary>
        public JatekElem[,] Elemek { get; set; }

        /// <summary>
        /// Gets or sets solution value
        /// </summary>
        public string Megoldas
        {
            get { return this.megoldas; }
            set { this.megoldas = value; }
        }

        /// <summary>
        /// Gets or sets filename value
        /// </summary>
        public string FajlNev
        {
            get { return this.fajlNev; }
            set { this.fajlNev = value; }
        }

        /// <summary>
        /// Gets or sets 'autok' value
        /// </summary>
        public List<ElhelyezettAuto> Autok
        {
            get { return this.autok; }
            set { this.autok = value; }
        }

        /// <summary>
        /// draw this object
        /// </summary>
        public void Kirajzol()
        {
            for (int i = 0; i < this.Elemek.GetLength(0); i++)
            {
                this.Elemek[0, i] = new Fal();
                this.Elemek[i, 0] = new Fal();
                this.Elemek[7, i] = new Fal();
                this.Elemek[i, 7] = new Fal();
            }

            this.Elemek[3, 7] = new Kijarat(3, 7);

            int size = ViewModel.TILESIZE;
            for (int i = 0; i < this.Elemek.GetLength(0); i++)
            {
                this.Elemek[i, 0].KirajzoloPont = new Point(0, i * size);
                this.Elemek[i, 0].Alak = new RectangleGeometry(new Rect(0, 0, size, size));

                this.Elemek[0, i].KirajzoloPont = new Point(i * size, 0);
                this.Elemek[0, i].Alak = new RectangleGeometry(new Rect(0, 0, size, size));

                this.Elemek[i, 7].KirajzoloPont = new Point(7 * size, i * size);
                this.Elemek[i, 7].Alak = new RectangleGeometry(new Rect(0, 0, size, size));

                this.Elemek[7, i].KirajzoloPont = new Point(i * size, 7 * size);
                this.Elemek[7, i].Alak = new RectangleGeometry(new Rect(0, 0, size, size));
            }

            foreach (ElhelyezettAuto auto in this.Autok)
            {
                auto.Kirajzol(auto.KezdoTombPont);
            }
        }

        /// <summary>
        /// sets items back to default
        /// </summary>
        public void VisszaAllit()
        {
            StreamReader palyaTxt = new StreamReader(this.FajlNev);
            this.Autok = new List<ElhelyezettAuto>();
            this.Megoldva = false;
            this.Elemek = new JatekElem[8, 8];
            while (!palyaTxt.EndOfStream)
            {
                string[] s = palyaTxt.ReadLine().Split(';');
                if (s[0] == "HS")
                {
                    this.Rekord = int.Parse(s[1]);
                }
                else if (s[0] == "S")
                {
                    this.Megoldas = s[1];
                }
                else
                {
                    // masolat az autorol h a tobbi palyan ne zavarjon
                    Auto auto = ViewModel.Get().Autok.Find(x => x.Nev.Contains(s[0]));
                    ElhelyezettAuto elhelyezettAuto;
                    switch (s[3])
                    {
                        case "F":
                            elhelyezettAuto = new ElhelyezettAuto(auto, Irany.Fel, new Point(int.Parse(s[1]), int.Parse(s[2])), this);
                            break;
                        case "L":
                            elhelyezettAuto = new ElhelyezettAuto(auto, Irany.Le, new Point(int.Parse(s[1]), int.Parse(s[2])), this);
                            break;
                        case "B":
                            elhelyezettAuto = new ElhelyezettAuto(auto, Irany.Bal, new Point(int.Parse(s[1]), int.Parse(s[2])), this);
                            break;
                        case "J":
                            elhelyezettAuto = new ElhelyezettAuto(auto, Irany.Jobb, new Point(int.Parse(s[1]), int.Parse(s[2])), this);
                            break;
                        default:
                            elhelyezettAuto = null;
                            break;
                    }

                    this.Autok.Add(elhelyezettAuto);
                }
            }

            palyaTxt.Close();
            this.Kirajzol();
        }

        /// <summary>
        /// saves map
        /// </summary>
        public void Ment()
        {
            StreamWriter sw = new StreamWriter(this.fajlNev);
            foreach (ElhelyezettAuto auto in this.Autok)
            {
                sw.Write(auto.Nev + ";" + auto.KezdoTombPont.X + ";" + auto.KezdoTombPont.Y + ";");
                switch (auto.Irany)
                {
                    case Irany.Fel:
                        sw.WriteLine("F");
                        break;
                    case Irany.Le:
                        sw.WriteLine("L");
                        break;
                    case Irany.Bal:
                        sw.WriteLine("B");
                        break;
                    case Irany.Jobb:
                        sw.WriteLine("J");
                        break;
                    default:
                        break;
                }
            }

            sw.WriteLine("S;" + this.megoldas);
            sw.Write("HS;" + this.Rekord);
            sw.Close();
        }
    }
}
