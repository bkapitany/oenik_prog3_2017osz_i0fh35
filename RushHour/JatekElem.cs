﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace RushHour
{
    /// <summary>
    /// game element class
    /// </summary>
    public abstract class JatekElem
    {
        /// <summary>
        /// geometry value
        /// </summary>
        private Geometry alak;

        /// <summary>
        /// point value
        /// </summary>
        private Point kirajzoloPont;

        /// <summary>
        /// color value
        /// </summary>
        private SolidColorBrush szin;
       
        /// <summary>
        /// Gets or sets the geometry
        /// </summary>
        public Geometry Alak
        {
            get
            {
                TranslateTransform tt = new TranslateTransform(this.kirajzoloPont.X, this.kirajzoloPont.Y);
                this.alak.Transform = tt;
                return this.alak.GetFlattenedPathGeometry();
            }

            set
            {
                this.alak = value;
            }
        }

        /// <summary>
        /// Gets or sets the color
        /// </summary>
        public SolidColorBrush Szin
        {
            get { return this.szin; }
            set { this.szin = value; }
        }

        /// <summary>
        /// Gets or sets the visualising point
        /// </summary>
        public Point KirajzoloPont
        {
            get { return this.kirajzoloPont; }

            set { this.kirajzoloPont = value; }
        }
    }
}
