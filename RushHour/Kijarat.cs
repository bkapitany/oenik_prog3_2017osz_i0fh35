﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace RushHour
{
    /// <summary>
    /// exit object
    /// </summary>
    public class Kijarat : JatekElem
    {   
        /// <summary>
        /// contructor of the exit
        /// </summary>
        /// <param name="i">koordinate one</param>
        /// <param name="j">koordinate two</param>
        public Kijarat(int i, int j)
        {
            int x = ViewModel.TILESIZE;
            KirajzoloPont = new Point(j * x, i * x);
            Alak = new RectangleGeometry(new Rect(0, 0, x, x));
            Szin = Brushes.White;
        }
    }
}
