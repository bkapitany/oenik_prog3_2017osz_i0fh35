﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Shapes;

namespace RushHour
{
    /// <summary>
    /// Converts a filename to readable string
    /// </summary>
    public class FajlNevConverter : IValueConverter
    {
        /// <summary>
        /// converts the value object to readable string
        /// </summary>
        /// <param name="value">object value</param>
        /// <param name="targetType">target type</param>
        /// <param name="parameter">object parameter</param>
        /// <param name="culture">culture info</param>
        /// <returns>a readable string</returns>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string s = (string)value;
            string valasz = string.Empty;
            foreach (char c in s)
            {
                if (char.IsDigit(c))
                {
                    valasz += c;
                }
            }          
              
            return valasz += ". pálya";
        }

        /// <summary>
        /// does nothing
        /// </summary>
        /// <param name="value">object value</param>
        /// <param name="targetType">target type</param>
        /// <param name="parameter">object parameter</param>
        /// <param name="culture">culture info</param>
        /// <returns>a readable string</returns>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }   
}
