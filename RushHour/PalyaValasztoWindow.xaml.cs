﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace RushHour
{
    /// <summary>
    /// Interaction logic for PalyaValasztoWindow.xaml
    /// </summary>
    public partial class PalyaValasztoWindow : Window
    {
        /// <summary>
        /// vievmodel object
        /// </summary>
        private ViewModel VM;

        /// <summary>
        /// initialize a map selecting window
        /// </summary>
        public PalyaValasztoWindow()
        {
            InitializeComponent();
            this.VM = ViewModel.Get();
            this.DataContext = this.VM;
        }

        /// <summary>
        /// next button
        /// </summary>
        /// <param name="sender">object sender</param>
        /// <param name="e">routed event arguments</param>
        private void btn_Tovabb(object sender, RoutedEventArgs e)
        {
            if (VM.AktualisPalya != null)
            {
                this.Close();
                JatekWindow jw = new JatekWindow();
                jw.ShowDialog();
            }           
        }

        /// <summary>
        /// back button
        /// </summary>
        /// <param name="sender">object sender</param>
        /// <param name="e">routed event arguments</param>
        private void btn_Vissza(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
