﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

/// <summary>
/// auto class & direction enum
/// </summary>
namespace RushHour
{ 
    /// <summary>
    /// autos direction
    /// </summary>
    public enum Irany
    {
        /// <summary>
        /// direction: up
        /// </summary>
        Fel,

        /// <summary>
        /// direction: down
        /// </summary>
        Le,

        /// <summary>
        /// direction: left
        /// </summary>
        Bal,

        /// <summary>
        /// direction: right
        /// </summary>
        Jobb
    }
    
    /// <summary>
    /// auto class
    /// </summary>
    public class Auto 
    {
        /// <summary>
        /// length of an auto
        /// </summary>
        private int hossz;

        /// <summary>
        /// name of an auto
        /// </summary>
        private string nev;

        /// <summary>
        /// color of an auto
        /// </summary>
        private SolidColorBrush szin;

        /// <summary>
        /// Constructor of th auto class
        /// </summary>
        public Auto()
        {
        }

        /// <summary>
        /// Gets or sets the color  
        /// </summary>
        public SolidColorBrush Szin
        {
            get { return this.szin; }
            set { this.szin = value; }
        }

        /// <summary>
        /// Gets or sets the name  
        /// </summary>
        public string Nev
        {
            get { return this.nev; }
            set { this.nev = value; }
        }    

        /// <summary>
        /// Gets or sets the lenght
        /// </summary>
        public int Hossz
        {
            get { return this.hossz; }
            set { this.hossz = value; }
        }
    }
}
