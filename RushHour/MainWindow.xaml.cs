﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace RushHour
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        /// <summary>
        /// viewmodel object
        /// </summary>
        private ViewModel VM;

        /// <summary>
        /// initializes a new mainwindow object
        /// </summary>
        public MainWindow()
        {
            InitializeComponent();
            VM = ViewModel.Get();
            this.DataContext = VM;            
        }

        /// <summary>
        /// play button
        /// </summary>
        /// <param name="sender">object sender</param>
        /// <param name="e">routed event arguments</param>
        private void btn_Jatek(object sender, RoutedEventArgs e)
        {
            VM.AktualisPalya = VM.Palyak[0];
            JatekWindow jw = new JatekWindow();
            jw.ShowDialog();
        }

        /// <summary>
        /// continue button
        /// </summary>
        /// <param name="sender">object sender</param>
        /// <param name="e">routed event arguments</param>
        private void btn_Folytatas(object sender, RoutedEventArgs e)
        {
            PalyaValasztoWindow pvw = new PalyaValasztoWindow();
            pvw.ShowDialog();
        }

        /// <summary>
        /// exit button
        /// </summary>
        /// <param name="sender">object sender</param>
        /// <param name="e">routed event arguments</param>
        private void btn_Kilepes(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// records button
        /// </summary>
        /// <param name="sender">object sender</param>
        /// <param name="e">routed event arguments</param>
        private void btn_Rekordok(object sender, RoutedEventArgs e)
        {
            RekordokWindow rw = new RekordokWindow();
            rw.Show();
        }
    }
}
