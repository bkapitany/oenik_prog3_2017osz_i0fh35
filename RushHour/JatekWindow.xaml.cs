﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace RushHour
{
    /// <summary>
    /// Interaction logic for JatekWindow.xaml
    /// </summary>
    public partial class JatekWindow : Window
    {
        /// <summary>
        /// viewmodel object
        /// </summary>
        private ViewModel VM;

        /// <summary>
        /// timer object
        /// </summary>
        private DispatcherTimer timer = new DispatcherTimer();

        /// <summary>
        /// stopwatch object
        /// </summary>
        private Stopwatch stopper;

        /// <summary>
        /// initializes this window
        /// </summary>
        public JatekWindow()
        {
            InitializeComponent();
            VM = ViewModel.Get();
            this.DataContext = VM.AktualisPalya;            
            timer.Interval = TimeSpan.FromMilliseconds(20);
            timer.Tick += Timer_Tick;
            timer.Start();
            stopper = new Stopwatch();
            stopper.Start();
        }

        /// <summary>
        /// timer method
        /// </summary>
        /// <param name="sender">object sender</param>
        /// <param name="e">event arguments</param>
        private void Timer_Tick(object sender, EventArgs e)
        {
            stopper_kijelzo.Content = (stopper.ElapsedMilliseconds / 1000) + " : "  + (stopper.ElapsedMilliseconds % 1000) + " mp";

            if (VM.AktualisPalya != default(Palya) && VM.AktualisPalya.Megoldva)
            {
                timer.Stop();
                stopper.Stop();
                if (stopper.ElapsedMilliseconds < VM.AktualisPalya.Rekord || VM.AktualisPalya.Rekord == 0)
                {
                    VM.AktualisPalya.Rekord = stopper.ElapsedMilliseconds;
                    VM.AktualisPalya.Ment();
                    VM.AktualisPalya.VisszaAllit();
                }

                if (MessageBox.Show("Következő pálya mehet?", "Szünet", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                {
                    VM.PalyaSzam++;

                    if (VM.PalyaSzam == VM.Palyak.Count)
                    {
                        VM.PalyaSzam = 0;
                    }  
                                      
                    VM.AktualisPalya = VM.Palyak[VM.PalyaSzam];
                    JatekWindow jw = new JatekWindow();
                    jw.Show();
                }   
                             
                this.Close();
            }
        }       

        /// <summary>
        /// giving up method
        /// </summary>
        /// <param name="sender">object sender</param>
        /// <param name="e">routed event arguments</param>
        private void btn_Feladom(object sender, RoutedEventArgs e)
        {
            stopper.Reset();
            timer.Stop();
            this.Close();
            VM.AktualisPalya.VisszaAllit();
            VM.PalyaSzam = 0;
        }

        /// <summary>
        /// solution button
        /// </summary>
        /// <param name="sender">object sender</param>
        /// <param name="e">routed event arguments</param>
        private void btn_Megoldas(object sender, RoutedEventArgs e)
        {
            stopper.Reset();
            VM.AktualisPalya.VisszaAllit();
            megoldas.Visibility = Visibility.Visible;
        }

        /// <summary>
        /// reset button
        /// </summary>
        /// <param name="sender">object sender</param>
        /// <param name="e">routed event arguments</param>
        private void btn_VisszaAllit(object sender, RoutedEventArgs e)
        {
            VM.AktualisPalya.VisszaAllit();
        }
    }
}
