﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;

namespace RushHour
{
    /// <summary>
    /// a class for placed autos
    /// </summary>
    public class ElhelyezettAuto : JatekElem
    {
        /// <summary>
        /// current koordinates (array)
        /// </summary>
        private Point aktPont;

        /// <summary>
        /// constructor of placed auto
        /// </summary>
        /// <param name="auto">original auto</param>
        /// <param name="irany">given direction</param>
        /// <param name="kezdoTP">starting point</param>
        /// <param name="tulaj">owner map</param>
        public ElhelyezettAuto(Auto auto, Irany irany, Point kezdoTP, Palya tulaj)
        {
            Hossz = auto.Hossz;
            Nev = auto.Nev;
            KezdoTombPont = kezdoTP;
            Szin = auto.Szin;
            TulajdonosPalya = tulaj;
            Irany = irany;

            TombBeir(KezdoTombPont);
        }

        /// <summary>
        /// Gets or sets the length
        /// </summary>
        public int Hossz { get; set; }
        
        /// <summary>
        /// Gets or sets the name
        /// </summary>
        public string Nev { get; set; }

        /// <summary>
        /// Gets or sets the owner (map)
        /// </summary>
        public Palya TulajdonosPalya { get; set; }

        /// <summary>
        /// Gets or sets the starting koordinates (array)
        /// </summary>
        public Point KezdoTombPont { get; set; }

        /// <summary>
        /// Gets or sets the current koordinates (array)
        /// </summary>
        public Point AktualisTombPont
        {
            get { return aktPont; }
            set { aktPont = value; }
        }

        /// <summary>
        /// Gets or sets the direction 
        /// </summary>
        public Irany Irany { get; set; }

        /// <summary>
        /// Gets or sets the koordinates (visualization)
        /// </summary>
        public new Point KirajzoloPont
        {
            get
            {
                return base.KirajzoloPont;
            }

            set
            {                
                Point ujTombPont = new Point((int)Math.Round(value.Y / ViewModel.TILESIZE), (int)Math.Round(value.X / ViewModel.TILESIZE));
                if (this.AktualisTombPont != ujTombPont)
                {
                    AktualisTombPont = ujTombPont;
                }

                base.KirajzoloPont = value;
            }
        }

        /// <summary>
        /// visualizing an auto
        /// </summary>
        /// <param name="pont">starting point</param>
        public void Kirajzol(Point pont)
        {
            int x = (int)pont.X;
            int y = (int)pont.Y;
            int size = ViewModel.TILESIZE;
            switch (this.Irany)
            {
                case Irany.Fel:
                case Irany.Le:
                    // itt nem kell modositani
                    this.KirajzoloPont = new Point(y * size, x * size);
                    this.Alak = new RectangleGeometry(new Rect(0, 0, size, size * this.Hossz));
                    break;
                case Irany.Bal:
                case Irany.Jobb:
                    // itt sem kell semmit modositani
                    this.KirajzoloPont = new Point(y * size, x * size);
                    this.Alak = new RectangleGeometry(new Rect(0, 0, size * this.Hossz, size));
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// moving an auto 
        /// </summary>
        /// <param name="honnan">staring point</param>
        /// <param name="hova">end point</param>
        /// <returns>true if it could move</returns>
        public bool Mozog(Point honnan, Point hova)
        {
            Vector eltolas = hova - honnan;
            Point regipont = AktualisTombPont;
            int size = ViewModel.TILESIZE;
            if (eltolas.X > size)
            {
                eltolas.X = size;
            }
            else if ((-1) * eltolas.X > size)
            {
                eltolas.X = (-1) * size;
            }

            if (eltolas.Y > size)
            {
                eltolas.Y = size;
            }
            else if ((-1) * eltolas.Y > size)
            {
                eltolas.Y = (-1) * size;
            }

            switch (this.Irany)
            {
                case Irany.Fel:
                case Irany.Le:
                    // lefele vagy felfele
                    if ((eltolas.Y > 0 && Keres(new Point(KirajzoloPont.X, KirajzoloPont.Y + (this.Hossz * ViewModel.TILESIZE) + 1))) ||
                        (eltolas.Y < 0 && Keres(new Point(KirajzoloPont.X, KirajzoloPont.Y - 1))))
                    {
                        // ha nincs alatta / felette semmi
                        KirajzoloPont += new Vector(0, eltolas.Y);
                        break;
                    }
                                        
                    break;                    
                case Irany.Bal:
                case Irany.Jobb:
                    // jobbra mozgatas 
                    if ((eltolas.X > 0 && Keres(new Point(KirajzoloPont.X + (this.Hossz * ViewModel.TILESIZE) + 1, KirajzoloPont.Y))) ||
                        (eltolas.X < 0 && Keres(new Point(KirajzoloPont.X - 1, KirajzoloPont.Y))))
                    {
                        // ha nincs jobbra semmi
                        KirajzoloPont += new Vector(eltolas.X, 0);
                        break;                        
                    }
                 
                    break;                    
                default:
                    break;           
            }

            if (AktualisTombPont != regipont)
            {
                TombTorol(regipont);
                TombBeir(AktualisTombPont);
                Kirajzol(AktualisTombPont);
            }

            return true;
        }

        /// <summary>
        /// deletes an auto from the owner's 'Elemek' array
        /// </summary>
        /// <param name="regiPont">old starting point</param>
        private void TombTorol(Point regiPont)
        {
            int x = (int)regiPont.X;
            int y = (int)regiPont.Y;
            for (int i = 0; i < this.Hossz; i++)
            {
                switch (this.Irany)
                {
                    case Irany.Fel:
                    case Irany.Le:
                        TulajdonosPalya.Elemek[x + i, y] = default(JatekElem);
                        break;
                    case Irany.Bal:
                    case Irany.Jobb:
                        TulajdonosPalya.Elemek[x, y + i] = default(JatekElem);
                        break;
                    default:
                        break;
                }
            }
        }

        /// <summary>
        /// looking for an object in the way of the placed auto
        /// </summary>
        /// <param name="kirajzoloPont">starting point</param>
        /// <returns>true if there is no element</returns>
        private bool Keres(Point kirajzoloPont)
        {
            // visszakonvertálás mert a tombben keresunk
            int x = (int)Math.Floor(kirajzoloPont.Y / ViewModel.TILESIZE);
            int y = (int)Math.Floor(kirajzoloPont.X / ViewModel.TILESIZE);

            if (x < 1 || x > TulajdonosPalya.Elemek.GetLength(0) - 1 || y < 1 || y > TulajdonosPalya.Elemek.GetLength(1) - 1)
            {
                return false;
            }

            if (TulajdonosPalya.Elemek[x, y] is Fal)
            {
                return false;
            }
            else if (TulajdonosPalya.Elemek[x, y] is Kijarat)
            {
                TulajdonosPalya.Megoldva = true;
            }
            else if (TulajdonosPalya.Elemek[x, y] == this)
            {
                return true;
            }
            else if (TulajdonosPalya.Elemek[x, y] == default(JatekElem))
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// write an auto into its owner's 'Elemek' array 
        /// </summary>
        /// <param name="pont">starting point</param>
        private void TombBeir(Point pont)
        {
            int x = (int)pont.X;
            int y = (int)pont.Y;
            for (int i = 0; i < this.Hossz; i++)
            {
                switch (this.Irany)
                {
                    case Irany.Fel:
                    case Irany.Le:
                        TulajdonosPalya.Elemek[x + i, y] = this;
                        break;
                    case Irany.Bal:
                    case Irany.Jobb:
                        TulajdonosPalya.Elemek[x, y + i] = this;
                        break;
                    default:
                        break;
                }
            }
        }
    }
}
